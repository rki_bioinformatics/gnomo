﻿# **gNOMO: a multi-omics pipeline for integrated host and microbiome analysis of non-model organisms**

Maria Muñoz-Benavent, Felix Hartkopf, Tim Van Den Bossche,  Vitor C. Piro, Carlos García-Ferris, Amparo Latorre, Bernhard Y. Renard & Thilo Muth.

- [Install](#Install)
- [Instructions](#Instructions)
- [Folder organisation](#Folder organisation)
- [gNOMO Workflow](#gNOMO Workflow)

## Install

### Miniconda:
```
# Change directory to your home directory
cd ~/

# Download conda installer
wget https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh

# Set permissions to execute
chmod +x Miniconda3-latest-Linux-x86_64.sh 	

# Execute. Make sure to "yes" to add the conda to your PATH
./Miniconda3-latest-Linux-x86_64.sh 		

# Add channels
conda config --add channels defaults
conda config --add channels conda-forge
conda config --add channels bioconda
```

### Snakemake:

```
conda install -c bioconda snakemake
```

* All other tools and dependencies are installed in their own environment automatically on the first run (with `--use-conda` parameter active).

### gNOMO:

Clone the gNOMO repository in a directory of your choosing.

```
git clone https://gitlab.com/gaspilleura/gnomo.git
```

## Instructions

- Make the `data` folder in your project folder.
- Then input files for metagenomics/metatranscriptomics should be located in the `Data` folder and metaproteomics files in the `data/MP/spectra` folder.
- Set parameters: First files to be configured should be config.yaml (configuration file: check parameters and input files names), metadata_MP.xlsx and grouping_info (with own metadata info for each kind of input).

Structure

```
Data
│
└───MG
│   │ (Metagenomics reads)
│
└───MT
│   │ (Metatranscriptomics reads)
│
└───MP
    |  
    └ databases
    |   | (Host database)
    |
    └ spectra
        | (MS/MS spectra)
```

**Set parameters**

Create a configuration file (config.yaml) with the required fields (take test configuration file config.yaml as an example):

```
#Set samples names for paired-end metagenomics and/or metatransriptomics samples 
samples: 
   "29":
      fq1: PATH/29_R1.fastq
      fq2: PATH/29_R2.fastq

#Set krona plots files names, write as much as number of metagenomics and/or metatranscriptomics samples
files:
   "1": PATH/TIGRFAM/krona1.txt

#Set archives names, which constitute the proteogenomcis database, write as much as number of metagenomics and/or metatranscriptomics samples
archives
   "1": PATH/MP/database/Prot1.fasta

#Set spectras names for metaproteomics samples
spectras:
   "10d1": PATH/MP/spectra/10d1.mgf

#Set counts names for krona plots of metaproteomics data, write as much as number of metaproteomics samples
counts:
   "1": PATH/MP/MSGF_PLUS/krona1.txt
```

At the end of the configuration file the parameters of the programs used in the pipeline can be setted. Standard parameters used in the test data are already included:

```
parameters:
    prinseq: "-trim_qual_step 5 -trim_qual_left 20 -trim_qual_right 20 -trim_qual_window 10 -min_qual_mean 20 -min_len 100 -out_bad null"
    augustus: "--species=pea_aphid --protein=on --gff3=on --uniqueGeneId=true --codingseq=on"
    msgf: "-t 25ppm -tda 1 -m 1 -inst 1 -e 1 -maxMissedCleavages 2"
```

Configuration file must be located in the `Project` folder.

**Set metadata files**

Create a metadata file (grouping_info) with the information of the metagenomics and metatranscriptomics samples:

| Name | Cond  | Data |
| :---: | :---:    | :---:  | 
| DNA29_Cond1 | Cond1 | DNA |
| DNA30_Cond1 | Cond1 | DNA |

And another metadata file for the metaproteomics samples (metadata_MP.xlsx):

| Name | Cond1  | Data |
| :---: | :---:  | :---: | 
| Bact_Prot_Cond1_1 | Cond1 | bact |
| Host_Prot_Cond1_1 | Cond1 | host |

**Download required files**

Download validation data from: [Validation data](https://zenodo.org/record/3569690#.Xfdvf9mCE5k)

Metagenonics and metatranscriptomics data (fastq files) must be located in the `Data` folder. Metaproteomics data (mgf files) must be located in the `Data/MP/Spectra` folder.

**Description of the validation data**

+ 2 x 6 MT and MG samples
+ 2 x 4 MP samples

Download folders `envs` and `scripts`, and the file Snakefile, all of them must be located inside the `Project` folder, together with the configuration file. 

**Run test data**

Run snakemake from your `Project` folder:

```
snakemake --use-conda
```

## Folder organisation

Flowchart with the **folders** organisation

```mermaid
graph TD;
A(Project) -->B(envs)
A --> C[my_interproscan]
C -->F[InterproScan Installation]
A -->D(scripts)
A -->E(data)
E-->G(Multiqc)
E-->H(sample)
H-->N(Kaiju)
H-->O(Ray)
E-->I(TIGRFAM)
I-->P(krona)
E-->J(TAXONOMY)
E-->K(host)
K-->Q(krona)
E-->L(eggnog)
E-->M(MP)
M-->R(database)
M-->S(spectra)
M-->T(MSGF_PLUS)
M-->U(Proteins)
```

Description with the content of each folder:

* **Project**: Snakefile, config file, metadata_MP.xlsx, grouping_info.xlsx, Kaiju db files. 
   -  **envs**: yaml files with envs. 
   - **my_interproscan**: Automatically installed. 
   - **Scripts**: python and R scripts used in the rules.  
   - **data**: INPUT files and cleaned files (fastq files)
      + **Multiqc**: MultiQC and FastQC files pre and post cleaning.
      + **{sample}**: one folder for each sample.
          - **Kaiju**: Kaiju outputs processed.
          - **Ray**: contigs, proteins predictions (Augustus and Prodigal). 
      + **Tigrfam**: LefSe for TIGRFAM annotation (roles and IDs) and Krona txts. 
          - **Krona**: Krona htmls.
      + **Taxonomy**: LefSe outputs, barplots and tavles of taxonomical annotation.  
      + **Host**: krona txts, eggnog and Lefse output of the host.
          - **Krona**: krona htmls of host functions. 
      + **eggnog**: eggnog output from microbiota, and pathview output from host and microbiota. 
      + **MP**: metaproteomics data.
          - **Database**: files for database constructions and the database.
          - **MSGF_PLUS**: LefSe files and tables with tax/func annotation of proteomic data.
          - **Spectra**: uploaded mgf files (INPUT).


## gNOMO Workflow
```mermaid
graph TD;
A[MG/MT input data] -->B[PrinSeq];
B --> |Cleaned reads|C{Fastq-join};
C -->|Joined reads to Taxonomy| D[Kaiju];
D -->|Taxonomy assigned|F[R];
C -->|Joined reads to Functions| E[Ray];
E -->|Assembly output|K{Augustus/Prodigal};
K-->|predicted proteins|M[InterproScan/EggNOG];
M-->|annotated sequences|L[R];
I[Spectra input]-->J[MSGF-PLUS output];
I-->|predicted proteins|K[Proteins];
K-->|Identified proteins/peptides|O[Unipept];
O-->|Annotated proteins|P[R];
P-->Q{Pathview};
L-->Q;
```


