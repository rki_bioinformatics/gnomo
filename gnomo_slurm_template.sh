#!/bin/bash
#
#SBATCH --job-name=gNOMO               # Job name, will show up in squeue output
#SBATCH --ntasks=<cores>               # Number of cores
#SBATCH --time=0-48:00:00              # Runtime in DAYS-HH:MM:SS format
#SBATCH --mem=200000                   # RAM (200 GB recommended)
#SBATCH --nodes=<nodes>                # Number of nodes
#SBATCH --output=gNOMO_%j.out          # File to which standard out will be written
#SBATCH --error=gNOMO_%j.err           # File to which standard err will be written
#SBATCH --mail-type=ALL                # Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=<email>            # Email to which notifications will be sent 
#SBATCH --qos=medium

cd <path/to/gnomo>
source $(conda info --root)/etc/profile.d/conda.sh
conda activate snakemake
snakemake --use-conda -j <cores>
conda deactivate
