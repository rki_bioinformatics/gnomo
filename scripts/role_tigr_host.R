## ---------------------------
##
## Script name: role_tigr_blattella.R
##
## Purpose of script:Assign TIGRFAM roles and subroles to host samples
##
## Author: Maria Mu�oz Benavent
##
## Date Created: March 2019
##
## Copyright (c) <Name>, 2018
## Email: Maria.Munoz-Benavent@uv.es
##
## ---------------------------
##
## Notes:
##   
##
## ---------------------------


# Load packages
library(readxl)
library(tidyr)
library(readr)
library(data.table)

# Get metadata info
grouping_info <- read_tsv("./grouping_info.tsv")
grouping_info<-as.data.frame(grouping_info)


# Get predermined table to assign role/subrole to IDs
Table_TIGRFAM <- read_tsv("Table_TIGRFAM.tsv")

# Get arguments from commandline
args = commandArgs(trailingOnly=TRUE)

# Set working directory
#print(paste(args[1],"/host",sep=""))
#setwd(paste(args[1],"/host",sep=""))
setwd("data/host/")

# Get file names
temp<-list.files(pattern = "tigr_IDs.tsv")

# Read all samples
for (i in 1:length(temp)) assign(temp[i], read.csv(temp[i], header=FALSE, sep="\t")) 

# Format TIGRFAM table
colnames(Table_TIGRFAM)<-c("Var1","ID","role","subrole")
Table_TIGRFAM2<-Table_TIGRFAM[,c(1,2)]
colnames(Table_TIGRFAM2)<-c("Var1","ID")
Table_TIGRFAM3<-unite(Table_TIGRFAM, roles, 3:4, sep="_", remove = TRUE)
Table_TIGRFAM3<-Table_TIGRFAM3[,-2]
Table_TIGRFAM<-Table_TIGRFAM2

# Indicate number of samples 
nsamples<-1:length(temp)
max_c<-(nchar(max(nsamples)))
numer <- c()
for(j in nsamples){
  numer <- c(numer,paste(paste(rep(0,max_c -nchar(nsamples[j])),collapse = ""),nsamples[j],sep=""))
}
# GENERATE ROL-SUBROL TABLE
# Make the tables with the TIGRFAm IDs and their frequencies
a <- paste("table",numer,sep="")
for(i in nsamples){
  assign(a[i],as.data.frame(table(get(temp[i]))))
}

b <- paste("merged",numer,sep="")
for(i in nsamples){
  assign(b[i],as.data.frame(merge(Table_TIGRFAM3,get(a[i]),by=c("Var1"))))
}

f <- paste("merged_tigr",numer,sep="")
for(i in nsamples){
  assign(f[i],as.data.frame(merge(Table_TIGRFAM,get(a[i]),by=c("Var1"))))
}

c<-paste("split",numer,sep="")
for (i in nsamples){
  assign(c[i],as.data.frame(do.call("rbind", strsplit(as.character(get(b[i])[,2]), ";"))))
}


g<-paste("split_tigr",numer,sep="")
for (i in nsamples){
  assign(g[i],as.data.frame(do.call("rbind", strsplit(as.character(get(f[i])[,1]), ";"))))
}

z<-paste("krona",numer,sep="")
for (i in nsamples){
  assign(z[i],as.data.frame(rbind(as.data.frame(get(b[i])[,c(3,2)]), ";")))
  csvfile<-paste0("krona",i,".txt")
  write.table(get(z[i]), csvfile, quote=FALSE, sep = "\t", row.names=FALSE)
}

# Final tables, has ID, role, subrole identifiers
d<-paste("final",numer,sep="")
for(i in nsamples){
  assign(d[i],as.data.frame(table(get(c[i]))))
}

h<-paste("final_tigr",numer,sep="")
for(i in nsamples){
  assign(h[i],as.data.frame(table(get(g[i]))))
}

# Join tables
e<-rbind(get(d[i])[,c(1,2)])

j<-rbind(get(h[i])[,c(1,2)])


# Get a list with all the samples information
l.df <- lapply(ls(pattern="final[0-9]+"), function(x) get(x))

# Fix vector e to merge all the tables of role/subrole in one table
e<-unique(e)
e<-as.data.frame(e[,1])
colnames(e)<-c("Var1")
e<-list(e)

final_e<-c(e, l.df)

TC<-unique(Reduce(function(...)merge(..., by="Var1",all.x=T), final_e))

# Fix vector j to merge all the tables of TIGRFAM IDs in one table
t.df <- lapply(ls(pattern="final_tigr[0-9]+"), function(x) get(x))

j<-unique(j)
j<-as.data.frame(j[,1])
colnames(j)<-c("Var1")
j<-list(j)

final_j<-c(j, t.df)

TC_tigr<-unique(Reduce(function(...)merge(..., by="Var1",all.x=T), final_j))


# Use irst column as rownames and remove it to only have numbers inside the dataframe

rownames(TC)<-TC[,1]
TC<-TC[,-1]
rownames(TC_tigr)<-TC_tigr[,1]
TC_tigr<-TC_tigr[,-1]

# Indicate the names of the samples (corresponding to the columns)
nombresmuestr<-grouping_info[,1]
colnames(TC)<-nombresmuestr
colnames(TC_tigr)<-nombresmuestr

# Remove NAs 
TC[is.na(TC)] <- 0
TC_tigr[is.na(TC_tigr)]<-0

# Normalize to 100
for(i in 1:dim(TC)[2]){
  TC[,i]<-TC[,i]*100/sum(TC[i])
}

for(i in 1:dim(TC_tigr)[2]){
  TC_tigr[,i]<-TC_tigr[,i]*100/sum(TC_tigr[i])
}

# Save Tables

write.csv(TC, "./TC_TIGRFAM_roles.tsv")
write.csv(TC_tigr, "./TC_TIGRFAM_IDs.tsv")


# Adapt the role/subrole table to be used in LefSe roles
LEFSE<-as.matrix(TC)
Lefse<-as.matrix(t(grouping_info))
colnames(Lefse)<-colnames(LEFSE)
lefse_test<-rbind(Lefse, LEFSE)

write.table(lefse_test, "./LEFSE_Table_roles.txt", quote = FALSE, col.names=FALSE,sep="\t")

# Adapt the TIGRFAM IDs table to be used in LefSe roles
LEFSE_t<-as.matrix(TC_tigr)
Lefse_t<-as.matrix(t(grouping_info))
colnames(Lefse_t)<-colnames(LEFSE_t)
lefse_tigr<-rbind(Lefse_t, LEFSE_t)

write.table(lefse_tigr, "./LEFSE_Table_ids.txt", quote = FALSE, col.names=FALSE,sep="\t")

