Project head    
Bernhard Y. Renard <Bernhard.Renard(at)hpi.de>   
Thilo Muth <thilo.muth(at)bam.de> 
Amparo Latorre <Amparo.Latorre(at)uv.es>
Carlos García-Ferris <Carlos.Garcia.Ferris(at)uv.es>

Technical head   
Maria Muñoz-Benavent <Maria.Munoz-Benavent(at)uv.es>   

Active Contributors   
Maria Muñoz-Benavent <Maria.Munoz-Benavent(at)uv.es>   
Felix Hartkopf <HartkopfF(at)rki.de>   
 
Former Contributors   
Tim Van Den Bossche   
Vitor C. Piro   