
import os
import yaml 

# Config file to store initial input data
configfile: "config.yaml"

# Path to the output directory
outputpath = config["outputpath"]

# Closest host species
specie = config["species"]

# KEGG Pathwyas selected
pathway = config["pathways"]

rule all:
    input:
        # Initial quality control and preprocessing of the input data
        multiqc_html = "data/Multiqc/multiqc_report.html",
        multiqc_cleaned_html = "data/Multiqc/cleaned/multiqc_report.html",
        fastq_all = expand("data/joined/{sample}/all.fastq", sample=config["samples"]),

        # Taxonomic classification using Kaiju
        kaiju_input = expand("data/kaiju_input/{sample}/ns.fastq", sample=config["samples"]),
        kaiju_output = expand("data/kaiju_output/{sample}/kaiju/ns_kaiju.out", sample=config["samples"]),
        
        # Taxonomic classification plots using Krona
        taxons = expand("data/kaiju_output/{sample}/kaiju/taxons", sample=config["samples"]), 
        krona_ns_out = expand("data/kaiju_output/{sample}/kaiju/ns_krona.out", sample=config["samples"]),
        cut_taxon_files = expand("data/TAXONOMY/{sample}names_taxa.tsv", sample=config["samples"]),
        kaiju_IDs = expand("data/kaiju_output/{sample}/kaiju/id", sample=config["samples"]),
        krona_files = expand("data/kaiju_output/{sample}/kaiju/krona.html", sample=config["samples"]),

        # Genome assembly using Ray 
        ray_output = expand("data/output/{sample}/Ray/Contigs.fasta", sample=config["samples"]),

        # Gene prediction using Augustus
        augustus_output = expand("data/output/{sample}/Ray/augustus_output.gff", sample=config["samples"]), 
        annotaeFasta = expand("data/output/{sample}/Ray/augustus_output.aa", sample=config["samples"]),

        # Proteins prediction using Prodigal
        protigalProteins = expand("data/output/{sample}/Ray/Proteinas.fasta",sample=config["samples"]),
        database_MP = "data/MP/database/database_MP.fasta",

        # Functional analysis using Interproscan
        interproscan_setup = "my_interproscan/interproscan_setup.txt",
        interproscan_run = expand("data/output/{sample}/Ray/TIGRFAM.tsv",sample=config["samples"]),
        interproscan_host = expand("data/output/{sample}/host/TIGRFAM.tsv",sample=config["samples"]),
        interproscan_post = expand("data/host/{sample}tigr_IDs.tsv",sample=config["samples"]),

        # Visualisation of functional analysis
        host_lefse = "data/host/LEFSE_Table_roles.txt",
        host_lefse_ids = "data/host/LEFSE_Table_ids.txt", 
        tigr_lefse="data/TIGRFAM/LEFSE_Table_roles.txt",
        tigr_lefse_ids_tig = "data/TIGRFAM/LEFSE_Table_ids.txt",
        tigrfam_files = expand("data/TIGRFAM/krona{file}.txt", file=config["files"]),

        # Eggnog analysis
        eggdb = "data/eggnog.db",
        diamond = "data/eggnog_proteins.dmnd",
        eggnog_output = expand("data/eggnog/{sample}eggnog.emapper.annotations",sample=config["samples"]),

        # Plot analyis
        krona_html = expand("data/host/Krona/krona{file}.html", file=config["files"]),
        tigr_krona_html = expand("data/TIGRFAM/Krona/krona{file}.html", file=config["files"]),
        lefse_files = "data/TIGRFAM/LEFSE_roles.png",
        lefse_id_files = "data/TIGRFAM/LEFSE_ids.png",
        lefse_Bla = "data/host/LEFSE_roles.png",
        lefse_ids = "data/host/LEFSE_ids.png",
        lefse_MP = "data/MP/Proteins/LEFSE_tax.in",
        lefse_genus = "data/TAXONOMY/LEFSE_taxRNA.png",
        lefse_prot = "data/MP/Proteins/LEFSE_MP_tax.png",

        # MSGFplus Analysis
        msgf_plus_db_search_host = expand("data/MP/MSGF_PLUS/{spectrum}_host.mzid", spectrum=config["spectra"]),
        msgf_plus_db_search_bact = expand("data/MP/MSGF_PLUS/{spectrum}_bact.mzid", spectrum=config["spectra"]),    
        msgf_plus_converts = expand("data/MP/MSGF_PLUS/{spectra}_{suf}.tsv", spectra=config["spectra"], suf=["bact","host"]),

        # Annotate peptides from database search
        peptides = expand("data/MP/MSGF_PLUS/peptides_{spectra}_{suf}.txt", spectra=config["spectra"], suf=["bact","host"]),
        
        # EGGNOG table
        eggnogTable = "data/MP/Proteins/ko03070.03070_log2ratioRNA_Prot_Host.multi.png"

rule fastqc:
    input:
        fq1 = lambda wildcards: config["samples"][wildcards.sample]["fq1"],
        fq2 = lambda wildcards: config["samples"][wildcards.sample]["fq2"]
    output:
        fastqc_html = "data/fastqc/{sample}/{sample}_{sufix}_fastqc.html",
        outzip = "data/fastqc/{sample}/{sample}_{sufix}_fastqc.zip"
    params:
        output_folder = "data/fastqc/{sample}/"
    conda:
        srcdir("envs/fastqc.yaml")    
    threads: 6
    shell:
        "fastqc -t {threads} {input.fq1} {input.fq2} -o {params} --extract"

rule multiqc:
    input:
        sample = expand("data/fastqc/{sample}/{sample}_{sufix}_fastqc.html", sample=config["samples"], sufix=["R1","R2"])
    output:
        output = "data/Multiqc/multiqc_report.html"
    params:
        input_dir = expand("data/fastqc/{sample}/", sample=config["samples"]),
        output_dir =  "data/Multiqc"
    conda:
        srcdir("envs/multiqc.yaml")
    shell:
        "multiqc {params.input_dir} -o {params.output_dir}"

rule prinseq:
    input:
        fq1 = lambda wildcards: config["samples"][wildcards.sample]["fq1"],
        fq2 = lambda wildcards: config["samples"][wildcards.sample]["fq2"]
    output:
        output1 = "data/cleaned/{sample}_cleaned_1.fastq",
        output2 = "data/cleaned/{sample}_cleaned_2.fastq"
    params:
        prefix = "data/cleaned/{sample}_cleaned",
        params = config["parameters"]["prinseq"]
    conda:
        srcdir("envs/prinseq.yaml")
    shell:
        """
        prinseq-lite.pl -fastq {input.fq1} -fastq2 {input.fq2} {params.params} -out_good {params.prefix}
        """

rule fastqc_cleaned:
    input:
        fq1 = "data/cleaned/{sample}_cleaned_1.fastq",
        fq2 = "data/cleaned/{sample}_cleaned_2.fastq"
    output:
        fastqc_cleaned_html = "data/cleaned/{sample}_cleaned_{sufix}_fastqc.html",
        outzip = "data/cleaned/{sample}_cleaned_{sufix}_fastqc.zip"
    params:
        output_folder = "data/cleaned/"
    conda:
        srcdir("envs/fastqc.yaml")
    threads: 6
    shell:
        "fastqc -t {threads} {input.fq1} {input.fq2} -o {params} --extract"

rule multiqc_cleaned:
    input:
        sample = expand("data/cleaned/{sample}_cleaned_{sufix}_fastqc.html", sample=config["samples"], sufix=["1","2"])
    output:
        output = "data/Multiqc/cleaned/multiqc_report.html"
    params:
        input_dir = "data/cleaned",
        output_dir = "data/Multiqc/cleaned"
    conda:
        srcdir("envs/multiqc.yaml")
    shell:
        "multiqc {params.input_dir} -o {params.output_dir}"

rule fastq_join:
    input:
        fq1 = "data/cleaned/{sample}_cleaned_1.fastq",
        fq2 = "data/cleaned/{sample}_cleaned_2.fastq"
    output:
        sample = "data/joined/{sample}/joined_join",
        un1 = "data/joined/{sample}/joined_un1",
        un2 = "data/joined/{sample}/joined_un2"
    params:
        output_prefix = "data/joined/{sample}/joined_"
    conda:
        srcdir("envs/fastq_join.yaml")
    shell:
        "fastq-join {input.fq1} {input.fq2} -o {params.output_prefix}"


rule cat:
    input:
        sample="data/joined/{sample}/joined_join",
        un1="data/joined/{sample}/joined_un1",
        un2="data/joined/{sample}/joined_un2"
    output:
        output="data/joined/{sample}/all.fastq"
    shell:
        "cat {input} > {output}"

rule sed:
    input:
        sample="data/joined/{sample}/all.fastq"
    output:
        output="data/kaiju_input/{sample}/ns.fastq"
    shell:
        "sed -r 's/\s+//g' {input} > {output}"


rule kaiju_db:
    output:
        nr = "data/nr/kaiju_db_nr.fmi",
        nodes = "data/nodes.dmp",
        names = "data/names.dmp"
    conda:
        srcdir("envs/kaiju.yaml")
    threads: 16
    shell:
        """
        cd data/
        kaiju-makedb -s nr -t {threads}
        """

rule kaiju:
    input:
        input="data/kaiju_input/{sample}/ns.fastq",
        nodes="data/nodes.dmp",
        fmi="data/nr/kaiju_db_nr.fmi"
    output:
        output="data/kaiju_output/{sample}/kaiju/ns_kaiju.out"
    conda:
        srcdir("envs/kaiju.yaml")
    log: "data/logs/kaiju/{sample}.log"
    threads: 16
    shell:
        """
        kaiju -t {input.nodes} -f {input.fmi} -i {input.input} -o {output} -z {threads}
        """

rule classify:
    input:
        sample = "data/kaiju_output/{sample}/kaiju/ns_kaiju.out"
    output:
        output = "data/kaiju_output/{sample}/kaiju/ns_class.out"
    shell:
        "grep -v ^U {input} > {output}"


rule addtaxonnames:
    input:
        sample="data/kaiju_output/{sample}/kaiju/ns_class.out",
        nodes="data/nodes.dmp",
        names="data/names.dmp"
    output:
        output="data/kaiju_output/{sample}/kaiju/taxons"
    conda:
        srcdir("envs/kaiju.yaml")
    shell:
        """
        kaiju-addTaxonNames -t {input.nodes} -n {input.names} -i {input.sample} -r phylum,family,genus -u -o {output}
        """

rule kaiju2krona:
    input: 
        sample="data/kaiju_output/{sample}/kaiju/taxons",
        names="data/names.dmp",
        nodes="data/nodes.dmp"
    output:
        output="data/kaiju_output/{sample}/kaiju/ns_krona.out"
    conda:
        srcdir("envs/kaiju.yaml")
    shell:
        "kaiju2krona -t {input.nodes} -n {input.names} -i {input.sample} -o {output}"

rule krona:
    input:
        sample = "data/kaiju_output/{sample}/kaiju/ns_krona.out"
    output:
        html = "data/kaiju_output/{sample}/kaiju/krona.html"
    conda:
        srcdir("envs/krona.yaml")
    shell:
        """
        for f in {input}
        do
        krona_files_names+="${{f}},${{f%%/*}} "
        done
        echo ${{krona_files_names}}
        ktImportText -o {output} ${{krona_files_names}}
        """


rule cut:
    input:
        sample = "data/kaiju_output/{sample}/kaiju/ns_class.out"
    output:
        output = "data/kaiju_output/{sample}/kaiju/id"
    shell:
        "cut -f 2 {input} > {output}"


rule cut_taxon:
    input: 
        sample = "data/kaiju_output/{sample}/kaiju/taxons"
    output:
        cut_taxon_files = "data/TAXONOMY/{sample}names_taxa.tsv"
    shell:
        "cut -f 4 {input} > {output}"


rule ray:
    input:
        sample = "data/kaiju_input/{sample}/ns.fastq"
    params:
        outdir = "data/output/{sample}/Ray/"
    output:
        output = "data/output/{sample}/Ray/Contigs.fasta"
    conda:
        srcdir("envs/Ray.yaml")
    threads: 16
    shell:
        "rm -r {params.outdir}; mpiexec -oversubscribe -n {threads} Ray -s {input} -o {params.outdir}"

rule augustus:
    input:
        sample = "data/output/{sample}/Ray/Contigs.fasta"
    output:
        output = "data/output/{sample}/Ray/augustus_output.gff"
    conda:
        srcdir("envs/augustus.yaml")
    params:
        params = config["parameters"]["augustus"]
    shell:
        "augustus {input} {params.params} > {output}"

rule AnnoFasta:
    input:
        sample = "data/output/{sample}/Ray/augustus_output.gff"
    output:
        output = "data/output/{sample}/Ray/augustus_output.aa"
    conda:
        srcdir("envs/perl.yaml")
    shell:
        "perl ./scripts/getAnnoFasta.pl {input}"

rule prodigal:
    input:
        sample = "data/output/{sample}/Ray/Contigs.fasta"
    output:
        proteins = "data/output/{sample}/Ray/Proteinas.fasta",
        genes = "data/output/{sample}/Ray/Genes.fasta",
        potential_genes = "data/output/{sample}/Ray/PotentialGenes.fasta",
        gbk = "data/output/{sample}/Ray/Prodigal_output.gbk"
    conda:
        srcdir("envs/prodigal.yaml")
    shell:
        "prodigal -i {input} -d {output.genes} -s {output.potential_genes} -p meta -o {output.gbk} -a {output.proteins}"

rule sed_prot:
    input:
        sample = "data/output/{sample}/Ray/Proteinas.fasta"
    output:
        output = "data/output/{sample}/Ray/ProtOK.fasta"
    shell:
        "sed 's/*//g' {input} > {output}"

rule sed_prot_db:
    input:
        sample = "data/output/{sample}/Ray/Proteinas.fasta"
    output:
        db = "data/MP/database/{sample}ProtOK.fasta"
    shell:
        "sed 's/*//g' {input} > {output}"

rule database_MP:
    input:
        sample = expand("data/MP/database/{sample}ProtOK.fasta", sample=config["samples"])
    output:
        output = "data/MP/database/Prot{archive}.fasta"
    conda:
        srcdir("envs/R.yaml")
    script:
        "./scripts/database_MP.R"

rule database:
    input:
        sample = expand("data/MP/database/Prot{archive}.fasta", archive=config["archives"])
    output:
        output = "data/MP/database/database_MP.fasta"
    shell:
        "cat {input} > {output}"


rule interproscan_setup:
    input:
        database = "data/MP/database/database_MP.fasta"
    output:
        output = "my_interproscan/interproscan_setup.txt"
    shell:
        """
        mkdir -p my_interproscan 
        cd my_interproscan
        wget ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/5.41-78.0/interproscan-5.41-78.0-64-bit.tar.gz >> interproscan_setup.txt
        wget ftp://ftp.ebi.ac.uk/pub/software/unix/iprscan/5/5.41-78.0/interproscan-5.41-78.0-64-bit.tar.gz.md5 >> interproscan_setup.txt

        # Recommended checksum to confirm the download was successful:
        md5sum -c interproscan-**.tar.gz.md5 >> interproscan_setup.txt
        # Must return *interproscan-**.tar.gz: OK*
        # If not - try downloading the file again as it may be a corrupted copy.
        tar -pxvzf interproscan-**.tar.gz >> interproscan_setup.txt
	"""

rule interproscan:
    input:
        sample = "data/output/{sample}/Ray/ProtOK.fasta",
        setup = "my_interproscan/interproscan_setup.txt"
    output:
        output = "data/output/{sample}/Ray/TIGRFAM.tsv"
    params:
        prefix = "data/output/{sample}/Ray/TIGRFAM"
    conda:
        srcdir("envs/interproscan.yaml")
    shell:
        "my_interproscan/interproscan*/interproscan.sh -appl TIGRFAM -pa -dra -b {params} -i {input}"

rule sed_fasta:
    input:
        sample = "data/output/{sample}/Ray/augustus_output.aa"
    output:
        output = "data/output/{sample}/host/proteins.fasta"
    shell:
        "sed 's/*//g' {input} > {output}"


rule sed_host_db:
    input:
        sample = "data/output/{sample}/host/proteins.fasta"
    output:
        db = "data/MP/database/{sample}ProtOK_host.fasta"
    shell:
        "sed 's/*//g' {input} > {output}"

rule database_MP_host:
    input:
        sample = expand("data/MP/database/{sample}ProtOK_host.fasta", sample=config["samples"])
    output:
        output = "data/MP/database/host/Prot{archive}.fasta"
    conda:
        srcdir("envs/R.yaml")
    script:
        "./scripts/database_MP.R"

rule database_host:
    input:
        sample = expand("data/MP/database/host/Prot{archive}.fasta", archive=config["archives"])
    output:
        output = "data/MP/database/database_MP_host.fasta"
    shell:
        "cat {input} > {output}"


rule inter_host:
    input:
        sample = "data/output/{sample}/host/proteins.fasta",
        setup = "my_interproscan/interproscan_setup.txt"
    output:
        file = "data/output/{sample}/host/TIGRFAM.tsv"
    params:
        prefix = "data/output/{sample}/host/TIGRFAM"
    conda:
        srcdir("envs/interproscan.yaml")
    shell:
        "my_interproscan/interproscan*/interproscan.sh -appl TIGRFAM -pa -dra -b {params} -i {input}"

rule cut_tigrfam:
    input:
        sample = "data/output/{sample}/Ray/TIGRFAM.tsv"
    output:
        interproscan_files = "data/TIGRFAM/{sample}tigr_IDs.tsv"
    shell:
        "cut -f 5 {input} > {output}"

rule cut_tigrfam_host:
    input:
        sample = "data/output/{sample}/host/TIGRFAM.tsv"
    output:
        output = "data/host/{sample}tigr_IDs.tsv"
    shell:
        "cut -f 5 {input} > {output}"

rule R_tigrfam:
    input:
        sample = expand("data/TIGRFAM/{sample}tigr_IDs.tsv", sample=config["samples"])
    output:
        lefse = "data/TIGRFAM/LEFSE_Table_roles.txt",
        lefse_ids_tig = "data/TIGRFAM/LEFSE_Table_ids.txt",
        krona_files = expand("data/TIGRFAM/krona{file}.txt", file=config["files"])
    conda:
        srcdir("envs/R.yaml")
    shell:
        "Rscript ./scripts/role_tigrfam.R"

rule R_tigr_host:
    input:
        sample=expand("data/host/{sample}tigr_IDs.tsv", sample=config["samples"])
    output:
        krona_host=expand("data/host/krona{file}.txt", file=config["files"]),
        lefse="data/host/LEFSE_Table_roles.txt",
        lefse_ids="data/host/LEFSE_Table_ids.txt"
    conda:
        srcdir("envs/R.yaml")
    shell:
        "Rscript ./scripts/role_tigr_host.R"

rule R_taxonomy:
    input:
        sample = expand("data/TAXONOMY/{sample}names_taxa.tsv", sample=config["samples"])
    output:
        TC_tax = "data/TAXONOMY/table_genus.tsv",
        lefse = "data/TAXONOMY/LEFSE_Table_genus.txt",
        lefseDNA = "data/TAXONOMY/LEFSE_Table_genusDNA.txt",
        lefseRNA = "data/TAXONOMY/LEFSE_Table_genusRNA.txt"
    conda:
        srcdir("envs/R.yaml")
    shell:
        "Rscript ./scripts/taxonomy_genus.R"

rule cut_krona:
    input:
        sample = "data/TIGRFAM/krona{file}.txt"
    output:
        output = "data/TIGRFAM/Krona/krona{file}_2.txt"
    shell:
        "sed -r 's/_/\t/g' {input} > {output}"

rule krona_tigr:
    input:
        sample = "data/TIGRFAM/Krona/krona{file}_2.txt"
    output:
        html = "data/TIGRFAM/Krona/krona{file}.html"
    conda:
        srcdir("envs/krona.yaml")
    shell:
        """
        for f in {input}
        do
        krona_files_names+="${{f}},${{f%%/*}} "
        done
        echo ${{krona_files_names}}
        ktImportText -o {output} ${{krona_files_names}}
        """

rule sed_host:
    input:
        sample = "data/host/krona{file}.txt"
    output:
        output = "data/host/Krona/krona{file}_2.txt"
    shell:
        "sed -r 's/_/\t/g' {input} > {output}"

rule sed_host2:
    input:
        sample = "data/host/Krona/krona{file}_2.txt"
    output:
        output = "data/host/Krona/krona{file}_3.txt"
    shell:
        "sed -r 's/\"//g' {input} > {output}"

rule krona_host:
    input:
        sample = "data/host/Krona/krona{file}_3.txt"
    output:
        html = "data/host/Krona/krona{file}.html"
    conda:
        srcdir("envs/krona.yaml")
    shell:
        "ktImportText {input} -o {output}"

rule eggnog_db:
    input: 
        sample = expand("data/output/{sample}/Ray/ProtOK.fasta", sample=config["samples"]),
    output:
        eggdb = "data/eggnog.db",
        diamond = "data/eggnog_proteins.dmnd",
    conda:
        srcdir("envs/eggnog.yaml")
    shell:
        "download_eggnog_data.py --data_dir ./data/  -y bact euk > ./data/eggnog_download.log 2>&1"

rule eggnog:
    input:
        sample = "data/output/{sample}/Ray/ProtOK.fasta",
        eggdb = "data/eggnog.db",
        diamond = "data/eggnog_proteins.dmnd"
    output:
        output = "data/eggnog/{sample}eggnog.emapper.annotations"
    params:
        prefix = "data/eggnog/{sample}eggnog"
    conda:
        srcdir("envs/eggnog.yaml")
    threads: 16
    shell:
        "emapper.py -o {params.prefix} -i {input.sample} --data_dir ./data/ -d bact -m diamond --dmnd_db {input.diamond} --cpu {threads} --resume"

rule eggnog_host:
    input:
        sample = "data/output/{sample}/host/proteins.fasta",
        db = "data/eggnog.db",
        diamond = "data/eggnog_proteins.dmnd"
    output:
        output = "data/host/{sample}eggnog.emapper.annotations"
    params:
        prefix = "data/host/{sample}eggnog"
    conda:
        srcdir("envs/eggnog.yaml")
    threads: 16
    shell:
        "emapper.py -o {params.prefix} -i {input.sample} --data_dir ./data/ -d euk -m diamond --dmnd_db {input.diamond} --cpu {threads} --resume"

rule lefse_format_input:
    input:
        sample = "data/TIGRFAM/LEFSE_Table_roles.txt"
    output:
        output = "data/TIGRFAM/LEFSE_roles.in"
    params:
        params = config["parameters"]["lefse"]
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "format_input.py {input} {output} {params.params} -o 1000000"

rule lefse_run:
    input:
        sample = "data/TIGRFAM/LEFSE_roles.in"
    output:
        output = "data/TIGRFAM/LEFSE_roles.res"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "run_lefse.py {input} {output}"

rule lefse_plot:
    input:
        sample = "data/TIGRFAM/LEFSE_roles.res"
    output:
        output = "data/TIGRFAM/LEFSE_roles.png"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "plot_res.py --dpi 400 {input} {output}"

rule lefse_format_input_id:
    input:
        sample = "data/TIGRFAM/LEFSE_Table_ids.txt"
    output:
        output = "data/TIGRFAM/LEFSE_ids.in"
    params:
        params = config["parameters"]["lefse"]
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "format_input.py {input} {output} {params.params} -o 1000000"

rule lefse_run_id:
    input:
        sample = "data/TIGRFAM/LEFSE_ids.in"
    output:
        output = "data/TIGRFAM/LEFSE_ids.res"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "run_lefse.py {input} {output}"

rule lefse_plot_id:
    input:
        sample = "data/TIGRFAM/LEFSE_ids.res"
    output:
        output = "data/TIGRFAM/LEFSE_ids.png"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "plot_res.py --dpi 400 {input} {output}"

rule lefse_format_input_host:
    input:
        sample = "data/host/LEFSE_Table_roles.txt"
    output:
        output = "data/host/LEFSE_roles.in"
    params:
        params = config["parameters"]["lefse"]
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "format_input.py {input} {output} {params.params} -o 1000000"

rule lefse_run_host:
    input:
        sample = "data/host/LEFSE_roles.in"
    output:
        output = "data/host/LEFSE_roles.res"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "run_lefse.py {input} {output}"

rule lefse_plot_host:
    input:
        sample = "data/host/LEFSE_roles.res"
    output:
        output = "data/host/LEFSE_roles.png"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "plot_res.py --dpi 400 {input} {output}"


rule lefse_format_input_host_id:
    input:
        sample = "data/host/LEFSE_Table_ids.txt"
    output:
        output = "data/host/LEFSE_ids.in"
    params:
        params = config["parameters"]["lefse"]
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "format_input.py {input} {output} {params.params} -o 1000000"

rule lefse_run_host_id:
    input:
        sample = "data/host/LEFSE_ids.in"
    output:
        output = "data/host/LEFSE_ids.res"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "run_lefse.py {input} {output}"

rule lefse_plot_host_id:
    input:
        sample = "data/host/LEFSE_ids.res"
    output:
        output = "data/host/LEFSE_ids.png"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "plot_res.py --dpi 400 {input} {output}"

rule lefse_format_input_tax:
    input:
        sample = "data/TAXONOMY/LEFSE_Table_genus.txt",
        input2 = "data/TAXONOMY/LEFSE_Table_genusDNA.txt",
        input3 = "data/TAXONOMY/LEFSE_Table_genusRNA.txt"
    output:
        output = "data/TAXONOMY/LEFSE_tax.in",
        output2 = "data/TAXONOMY/LEFSE_taxDNA.in",
        output3 = "data/TAXONOMY/LEFSE_taxRNA.in" 
    params:
        params = config["parameters"]["lefse"]
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        """
        format_input.py {input.sample} {output.output} {params.params} -o 1000000
        format_input.py {input.input2} {output.output2} {params.params} -o 1000000
        format_input.py {input.input3} {output.output3} {params.params} -o 1000000
        """

rule lefse_tax_2:
    input:
        sample = "data/TAXONOMY/LEFSE_tax.in",
        input2 = "data/TAXONOMY/LEFSE_taxDNA.in",
        input3 = "data/TAXONOMY/LEFSE_taxRNA.in"
    output:
        output = "data/TAXONOMY/LEFSE_tax.res",
        output2 = "data/TAXONOMY/LEFSE_taxDNA.res",
        output3 = "data/TAXONOMY/LEFSE_taxRNA.res"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        """
        run_lefse.py {input.sample} {output.output}
        run_lefse.py {input.input2} {output.output2}
        run_lefse.py {input.input3} {output.output3}
        """

rule lefse_tax_3:
    input:
        sample = "data/TAXONOMY/LEFSE_tax.res",
        input2 = "data/TAXONOMY/LEFSE_taxDNA.res",
        input3 = "data/TAXONOMY/LEFSE_taxRNA.res"
    output:
        output = "data/TAXONOMY/LEFSE_tax.png",
        output2 = "data/TAXONOMY/LEFSE_taxDNA.png",
        output3 = "data/TAXONOMY/LEFSE_taxRNA.png"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        """
        plot_res.py --dpi 400 {input.sample} {output.output}
        plot_res.py --dpi 400 {input.input2} {output.output2}
        plot_res.py --dpi 400 {input.input3} {output.output3}
        """

rule msgf_host:
    input:
        input = "data/MP/spectra/{spectrum}.mgf",
        db = "data/MP/database/database_MP_host.fasta",
    output:
        output="data/MP/MSGF_PLUS/{spectrum}_host.mzid", 
    conda:
        srcdir("envs/msgfplus.yaml")
    params:
        params = config["parameters"]["msgf"]
    shell:
        """
        msgf_plus -Xmx30000M -s {input.input} -d {input.db} {params.params} -o {output.output} 
        """

rule msgf_bact:
    input:
        input = "data/MP/spectra/{spectrum}.mgf",
	    db = "data/MP/database/database_MP.fasta" 
    output:
        output="data/MP/MSGF_PLUS/{spectrum}_bact.mzid", 
    conda:
        srcdir("envs/msgfplus.yaml")
    params:
        params = config["parameters"]["msgf"]
    shell:
        """
        msgf_plus -Xmx30000M -s {input.input} -d {input.db} {params.params} -o {output.output}
        """

rule mzid:
    input:
        input = "data/MP/MSGF_PLUS/{spectra}_{suf}.mzid"
    output:
        output = "data/MP/MSGF_PLUS/{spectra}_{suf}.tsv"
    conda:
        srcdir("envs/msgfplus.yaml") 
    shell:
        "msgf_plus edu.ucsd.msjava.ui.MzIDToTsv -Xmx3000M -i {input.input} -o {output.output}"


rule process_unipept:
    input:
        input = "data/MP/MSGF_PLUS/{spectra}_{suf}.tsv"
    output:
        output = "data/MP/MSGF_PLUS/peptides_{spectra}_{suf}.txt"
    shell:
        """
        awk 'BEGIN{{FS="\t"; OFS=FS;}} $16<0.05 {{ print; }}' {input} | cut -f10 | sed 's/^.\.//' | sed 's/[0-9\+,]//g' | sed 's/\..$//' | sed 's/\.//g'  > {output}
        """

rule unipept:
    input:
        input="data/MP/MSGF_PLUS/peptides_{spectra}_{suf}.txt"
    output:
        output="data/MP/Proteins/taxec_{spectra}_{suf}.csv"
    conda:
        srcdir("envs/pyteomics.yaml")
    shell:
        "python scripts/unipept-get-peptinfo.py -i ./{input.input} -o ./{output}"

rule processing_prot:
    input:
        input="data/MP/Proteins/taxec_{spectra}_{suf}.csv"
    output:
        out1="data/MP/Proteins/names_taxa_{spectra}_{suf}.txt",
        out2="data/MP/Proteins/names_ec_{spectra}_{suf}.txt"
    shell:
        "cut -f2,3,4 -d, {input} > {output.out1} | cut -f5 -d, {input} > {output.out2}"

rule tax_prot:
    input:
        input=expand("data/MP/Proteins/names_taxa_{spectra}_{suf}.txt", spectra=config["spectra"], suf=["bact","host"])
    output:
        output=expand("data/MP/Proteins/krona{count}.txt", count=config["counts"]), 
        lefse="data/MP/Proteins/LEFSE_MP_genus.txt"
    conda:
        srcdir("envs/R.yaml")
    shell:
        "Rscript ./scripts/process_MP.R"

rule krona_prot:
    input:
        sample = "data/MP/Proteins/krona{count}.txt"
    output:
        html = "data/MP/Proteins/krona{count}.html"
    conda:
        srcdir("envs/krona.yaml")
    shell:
        """
        for f in {input}
        do
        krona_files_names+="${{f}},${{f%%/*}} "
        done
        echo ${{krona_files_names}}
        ktImportText -o {output} ${{krona_files_names}}
        """

rule lefse_format_input_prot:
    input:
        sample = "data/MP/Proteins/LEFSE_MP_genus.txt"
    output:
        output = "data/MP/Proteins/LEFSE_tax.in"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "format_input.py {input} {output} -c 3 -s 2 -u 1 -o 1000000"

rule lefse_run_prot:
    input:
        sample = "data/MP/Proteins/LEFSE_tax.in"
    output:
        output = "data/MP/Proteins/LEFSE_tax.res"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "run_lefse.py {input} {output}"

rule lefse_plot_prot:
    input:
        sample = "data/MP/Proteins/LEFSE_tax.res"
    output:
        output = "data/MP/Proteins/LEFSE_MP_tax.png"
    conda:
        srcdir("envs/lefse.yaml")
    shell:
        "plot_res.py --dpi 400 {input} {output}"


rule table_eggnog:
    input:
        sample = expand("data/eggnog/{sample}eggnog.emapper.annotations", sample=config["samples"]),
        sample_host = expand("data/host/{sample}eggnog.emapper.annotations", sample=config["samples"]),
        sample_MP = expand("data/MP/Proteins/names_ec_{spectra}_{suf}.txt", spectra=config["spectra"], suf=["bact","host"])
    output:
        output = "data/MP/Proteins/ko03070.03070_log2ratioRNA_Prot_Host.multi.png"
    conda:
        srcdir("envs/R.yaml")
    params:
        species = config["species"],
        pathways = config["pathways"]
    shell:
        "Rscript ./scripts/integration.R {params.species} {params.pathways}"

